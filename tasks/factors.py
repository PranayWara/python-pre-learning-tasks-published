def factors(number):
    # ==============
    prime = True
    print('factors for', number, 'are')
    for i in range(2, number):
        if number % i == 0:
            prime = False
            print(i)
    if prime:
        print('prime')
#test
    # ==============

factors(15) # Should print [3, 5] to the console
factors(12) # Should print [2, 3, 4, 6] to the console
factors(13) # Should print “[]” (an empty list) to the console
